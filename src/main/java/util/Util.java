/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import com.api.restservice.playload.AvionRequest;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.FileUtils;
import org.springframework.web.multipart.MultipartFile;
//import org.apache.commons.;

/**
 *
 * @author mariano
 */
public class Util {
    private static final String imagesFolderPath = "/resources/images";
	private static final String imageExtensionServer = ".jpg";
	private static final String[] extensionsAllowed = { "jpg", "jpeg", "png" };
    
    public static String getCurrentDate() {
        LocalDate date = java.time.LocalDate.now();
        return date.toString();
    }

    public static boolean validateJavaDate(String strDate) {
        /* Check if date is 'null' */
        if (strDate.trim().equals("")) {
            return true;
        } /* Date is not 'null' */ else {
            /*
	     * Set preferred date format,
	     * For example MM-dd-yyyy, MM.dd.yyyy,dd.MM.yyyy etc.*/
            SimpleDateFormat sdfrmt = new SimpleDateFormat("yyyy-MM-dd");
            sdfrmt.setLenient(false);
            /* Create Date object
	     * parse the string into date 
             */
            try {
                Date javaDate = sdfrmt.parse(strDate);

            } /* Date format is invalid */ catch (ParseException e) {

                return false;
            }
            /* Return true if date format is valid */
            return true;
        }
    }

    public static String formatDateBegin(String d) throws Exception {
        String date = d;
        if (d.equals("")) {
            date = getCurrentDate();
        }
        if (validateJavaDate(date)) {
            return date + " 00:00:00";
        } else {
            throw new Exception("invalid date format");
        }
    }
    

    public static String formatDateEnd(String d) throws Exception {
        String date = d;
        if (d.equals("")) {
            date = getCurrentDate();
        }
        if (validateJavaDate(date)) {
            return date + " 23:59:59";
        } else {
            throw new Exception("invalid date format");
        }
    }
    public static String formatDateBeginExtrem(String d) throws Exception {
        String date = d;
        if ( d == null || d.equals("") ) {
            date = "1970-01-01";
        }
        if (validateJavaDate(date)) {
            return date + " 00:00:00";
        } else {
            throw new Exception("invalid date format");
        }
    }
    

    public static String formatDateEndExtrem(String d) throws Exception {
        String date = d;
        if (d == null || d.equals("")) {
            date = "3020-12-12";
        }
        if (validateJavaDate(date)) {
            return date + " 23:59:59";
        } else {
            throw new Exception("invalid date format");
        }
    }

    public static int timeTosec(Time t) {
        String heure = t.toString();
        String[] heures = heure.split(":");
        int hr = Integer.parseInt(heures[0]) * 3600;
        int mn = Integer.parseInt(heures[1]) * 60;
        int sc = Integer.parseInt(heures[2]);
        return hr + mn + sc;   
    }
    public static Timestamp addSectoTs(Timestamp original, Time t){
        int sec = timeTosec(t);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(original.getTime());
        cal.add(Calendar.SECOND, sec);
        return new Timestamp(cal.getTime().getTime());
    }
    public static String showDate(Timestamp date){
         String dayName = (new SimpleDateFormat("EEEE")).format(date.getTime());
         String month = (new SimpleDateFormat("MMMM")).format(date.getTime());
         int day = date.getDate();
         int year = date.getYear();
         int hour = date.getHours();String h = hour+"";
         if (hour < 10){h = "0"+h;}
         int min = date.getMinutes();String m = min+"";
         if (min < 10){m = "0"+m;}
         int sec = date.getSeconds();String s = sec+"";
         if (sec < 10){s = "0"+s;}
         
         return dayName+", "+day+" "+month+" "+(year+1900)+" à "+h+":"+m+":"+s;
    }
    public void uploadProductImage(AvionRequest avion, HttpSession session) throws IOException
			 {
		MultipartFile image = avion.getImg();
		if(image == null) {
			throw new RuntimeException("pas d'image");
		}
		
		String[] typeExtension = image.getContentType().split("/");
		String type = typeExtension[0];
		if (!type.equals("image")) {
			throw new RuntimeException("image error");
		}
		String extension = typeExtension[1];
		boolean extensionCorrect = false;
		for (int j = 0; j < extensionsAllowed.length; j++) {
			if (extensionsAllowed[j].equals(extension)) {
				extensionCorrect = true;
				break;
			}
		}
		if (!extensionCorrect) {
			throw new RuntimeException("image error");
		}

//		String avionNameUpScore = StringUtils.convertSpaceToUpperScore(avion.getNom());
		String avionNameUpScore = avion.getNomAvion().replaceAll(" ", "-");
		File avionDirectory = new File(
				session.getServletContext().getRealPath(imagesFolderPath + "/" + avionNameUpScore));
//		System.out.println(productDirectory.getCanonicalPath());
		FileUtils.deleteDirectory(avionDirectory);
		Files.createDirectories(
				Paths.get(session.getServletContext().getRealPath(imagesFolderPath + "/" + avionNameUpScore)));
		
		Path rootLocation = Paths
				.get(session.getServletContext().getRealPath(imagesFolderPath + "/" + avionNameUpScore));
		
		Files.copy(image.getInputStream(),
				rootLocation.resolve(avionNameUpScore.toLowerCase() + imageExtensionServer));
	}
}
