/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.restservice.repository;

import com.api.restservice.model.Vol;
import java.sql.Timestamp;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author mariano
 */
public interface VolRepository extends JpaRepository<Vol, String> {
    @Query(value = "select * from vol where date_vol between :datedeb and :datefin and id_tr like %:idtr% and id_avn like %:id_avn% and type_vol = :typ", nativeQuery = true)
    List<Vol> findFilter(@Param("datedeb")Timestamp date1,@Param("datefin") Timestamp date2,@Param("idtr") String idtr,@Param("id_avn")String idavn,@Param("typ")int typ);
    @Query(value = "select * from vol where date_vol between :datedeb and :datefin and id_tr like %:idtr% and id_avn like %:id_avn% ", nativeQuery = true)
    Page<Vol> findFilterwithoutType(@Param("datedeb")Timestamp date1,@Param("datefin") Timestamp date2,@Param("idtr") String idtr,@Param("id_avn")String idavn, Pageable pageable);
}
