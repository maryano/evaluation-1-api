/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.restservice.repository;

import com.api.restservice.model.VolInfo;
import java.sql.Timestamp;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author mariano
 */
public interface VolInfoRepository extends JpaRepository<VolInfo, String>{
    @Query(value = "select * from vol_avn_tr where date_vol between :datedeb and :datefin and ville_dep like %:idtr% and id_avn like %:id_avn% ", nativeQuery = true)
    Page<VolInfo> findFilterwithoutType(@Param("datedeb")Timestamp date1,@Param("datefin") Timestamp date2,@Param("idtr") String idtr,@Param("id_avn")String idavn, Pageable pageable);
}
