/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.restservice.repository;

import com.api.restservice.model.Avion;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author mariano
 */
public interface AvionRepository extends JpaRepository<Avion, String> {

    Page<Avion> findAll(Pageable pageable);

    @Query(value = "select * from avion where nom_avn like %:nom% and id_mdl like %:mdl% ", nativeQuery = true)
    List<Avion> findFilter(@Param("nom")String nom,@Param("mdl") String modele);

}
