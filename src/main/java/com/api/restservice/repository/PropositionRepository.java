/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.restservice.repository;

import com.api.restservice.model.Proposition;
import com.api.restservice.model.PropositionId;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author mariano
 */
public interface PropositionRepository extends JpaRepository<Proposition,PropositionId>{
//    List<Proposition>findDate_volBetween(String date1,String date2);
}
