/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.restservice.playload;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 *
 * @author mariano
 */
public class VolRequest {

    @NotBlank
    private String dateVol;
    @NotBlank
    private String time;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @NotNull
    private Integer typeVol;
    @NotBlank
    private String idTr;
    @NotBlank
    private String idAvn;
    
    
    
    
    

    public String getDateVol() {
        return dateVol;
    }

    public void setDateVol(String dateVol) {
        this.dateVol = dateVol;
    }

    public int getTypeVol() {
        return typeVol;
    }

    public void setTypeVol(int typeVol) {
        this.typeVol = typeVol;
    }

    public String getIdTr() {
        return idTr;
    }

    public void setIdTr(String idTr) {
        this.idTr = idTr;
    }

    public String getIdAvn() {
        return idAvn;
    }

    public void setIdAvn(String idAvn) {
        this.idAvn = idAvn;
    }

   

}
