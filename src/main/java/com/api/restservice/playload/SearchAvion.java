/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.restservice.playload;

/**
 *
 * @author mariano
 */
public class SearchAvion {
    private String idMdl;
    private String nomAvn;

    public String getIdMdl() {
        return idMdl;
    }

    public void setIdMdl(String idMdl) {
        this.idMdl = idMdl;
    }

    public String getNomAvn() {
        return nomAvn;
    }

    public void setNomAvn(String nomAvn) {
        this.nomAvn = nomAvn;
    }
    
}
