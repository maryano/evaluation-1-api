/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.restservice.playload;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author mariano
 */
public class AvionRequest {
    @NotNull
    @Size(min=2, max=30)
    private String nomAvion;
    @NotNull
    private String idMdl;
    
    private MultipartFile img;
    
    

    public String getNomAvion() {
        return nomAvion;
    }

    public void setNomAvion(String nomAvion) {
        this.nomAvion = nomAvion;
    }

    public String getIdMdl() {
        return idMdl;
    }

    public void setIdMdl(String idMdl) {
        this.idMdl = idMdl;
    }

    public MultipartFile getImg() {
        return img;
    }

    public void setImg(MultipartFile img) {
        this.img = img;
    }
    
    
}
