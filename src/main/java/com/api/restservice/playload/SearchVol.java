/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.restservice.playload;

/**
 *
 * @author mariano
 */
public class SearchVol {
    String debut;
    String fin;
    String idTr;
    String idAvn;
    String sort;

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
    
    

    public String getDebut() {
        return debut;
    }

    public void setDebut(String debut) {
        this.debut = debut;
    }

    public String getFin() {
        return fin;
    }

    public void setFin(String fin) {
        this.fin = fin;
    }

    public String getIdTr() {
        return idTr;
    }

    public void setIdTr(String idTr) {
        this.idTr = idTr;
    }

    public String getIdAvn() {
        return idAvn;
    }

    public void setIdAvn(String idAvn) {
        this.idAvn = idAvn;
    }

    
    
    
}
