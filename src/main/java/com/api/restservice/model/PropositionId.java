/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.restservice.model;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author mariano
 */
public class PropositionId implements Serializable{
    private String id;
    private String idPst;

    public PropositionId(String id, String idPst) {
        this.id = id;
        this.idPst = idPst;
    }
    public PropositionId() {
        
    }

    public String getIdPst() {
        return idPst;
    }

    public void setIdPst(String idPst) {
        this.idPst = idPst;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
     @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PropositionId propId = (PropositionId) o;
        return id.equals(propId.id) &&
                idPst.equals(propId.idPst);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, idPst);
    }
    
}
