/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools ;
 Templates
 * and open the template in the editor.
 */
package com.api.restservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author mariano
 */
@Entity
public class Modele {
    @Id
    private String id;
    @Column(name="nom_mdl")
    private String nom_mdl;
    @Column(name="longueur_mdl")
    private float longueur_mdl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom_mdl() {
        return nom_mdl;
    }

    public void setNom_mdl(String nom_mdl) {
        this.nom_mdl = nom_mdl;
    }

    public float getLongueur_mdl() {
        return longueur_mdl;
    }

    public void setLongueur_mdl(float longueur_mdl) {
        this.longueur_mdl = longueur_mdl;
    }
    
    
}
