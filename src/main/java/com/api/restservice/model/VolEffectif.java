/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.restservice.model;

import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import util.HbUtil;

/**
 *
 * @author mariano
 */
@Entity
@Table(name = "vol_effectif")
public class VolEffectif {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    private Timestamp date_effectif;
    private String id_vol;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Timestamp getDate_effectif() {
        return date_effectif;
    }

    public void setDate_effectif(Timestamp date_effectif) {
        this.date_effectif = date_effectif;
    }

    public String getId_vol() {
        return id_vol;
    }

    public void setId_vol(String id_vol) {
        this.id_vol = id_vol;
    }

    //must set VE's attributes before SAVE
    public String save(Timestamp date) throws Exception{
        SessionFactory factory = HbUtil.getSessionFactory();
        Session session = factory.openSession();
        String id = "";
        try {
            id = this.save(date, session);
        } catch (HibernateException e) {

            throw e;
        } finally {
            session.close();
        }
        return id;
    }

    public String save(Timestamp date, Session session)throws Exception {
        if (date == null) {
            date = new Timestamp(System.currentTimeMillis());
        }
        Transaction tx = null;
        String ID = null;
        try {
            this.setDate_effectif(date);
            tx = session.beginTransaction();
            ID = (String) session.save(this);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            throw e;
        }
        return ID;
    }

}
