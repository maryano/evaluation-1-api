/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools ;
 Templates
 * and open the template in the editor.
 */
package com.api.restservice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.sql.Time;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 *
 * @author mariano
 */
@Entity
@Table(name = "vol_avn_tr")
public class VolInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    @NotNull
    @Column(name="date_vol")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Timestamp dateVol;
    @Column(name="decalage_vol")
    private Time decalageVol;
    @Column(name="etat_decalage_vol")
    private int etatDecalageVol;
    @Column(name="etat_vol")
    private int etatVol;
    @NotNull
    @Column(name="type_vol")
    private int typeVol;
    @NotNull
    @NotBlank
    @Column(name="id_tr")
    private String idTr;
    @NotNull
    @NotBlank   
    @Column(name="id_avn")
    private String idAvn;
    @Column(name = "nom_avn")
    private String nomAvn;
    @Column(name = "id_mdl")
    private String idMdl;
    @Column(name = "nom_mdl")
    String nom_mdl ;
    @Column(name="longueur_mdl")
     private double longueurMdl;
    @Column(name="ville_dep")
    String villeDep ;
    
    String typevollabel;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Timestamp getDateVol() {
        return dateVol;
    }

    public void setDateVol(Timestamp dateVol) {
        this.dateVol = dateVol;
    }

    public Time getDecalageVol() {
        return decalageVol;
    }

    public void setDecalageVol(Time decalageVol) {
        this.decalageVol = decalageVol;
    }

    public int getEtatDecalageVol() {
        return etatDecalageVol;
    }

    public void setEtatDecalageVol(int etatDecalageVol) {
        this.etatDecalageVol = etatDecalageVol;
    }

    public int getEtatVol() {
        return etatVol;
    }

    public void setEtatVol(int etatVol) {
        this.etatVol = etatVol;
    }

    public int getTypeVol() {
        return typeVol;
    }

    public void setTypeVol(int typeVol) {
        this.typeVol = typeVol;
    }

    public String getIdTr() {
        return idTr;
    }

    public void setIdTr(String idTr) {
        this.idTr = idTr;
    }

    public String getIdAvn() {
        return idAvn;
    }

    public void setIdAvn(String idAvn) {
        this.idAvn = idAvn;
    }

    public String getNomAvn() {
        return nomAvn;
    }

    public void setNomAvn(String nomAvn) {
        this.nomAvn = nomAvn;
    }

    public String getIdMdl() {
        return idMdl;
    }

    public void setIdMdl(String idMdl) {
        this.idMdl = idMdl;
    }

    public String getNom_mdl() {
        return nom_mdl;
    }

    public void setNom_mdl(String nom_mdl) {
        this.nom_mdl = nom_mdl;
    }

    public double getLongueurMdl() {
        return longueurMdl;
    }

    public void setLongueurMdl(double longueurMdl) {
        this.longueurMdl = longueurMdl;
    }

    public String getVilleDep() {
        return villeDep;
    }

    public void setVilleDep(String villeDep) {
        this.villeDep = villeDep;
    }

    public String getTypevollabel() {
        return typevollabel;
    }

    public void setTypevollabel(String typevollabel) {
        this.typevollabel = typevollabel;
    }
    
   
}
