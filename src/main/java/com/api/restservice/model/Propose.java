/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.restservice.model;

/**
 *
 * @author mariano
 */
import javax.persistence.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import util.HbUtil;

/**
 *
 * @author mariano
 */
@Entity
@Table(name = "Propose")
public class Propose {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    String id;
    @Column(name="id_vol")
    String idVol ;
    @Column(name="id_pst")
    String idPst ;
//    private Timestamp real_date_vol;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdVol() {
        return idVol;
    }

    public void setIdVol(String idVol) {
        this.idVol = idVol;
    }

    public String getIdPst() {
        return idPst;
    }

    public void setIdPst(String idPst) {
        this.idPst = idPst;
    }
    public void delete(String date1, String date2){
      SessionFactory factory = HbUtil.getSessionFactory();
      Session session = factory.openSession();
      Transaction tx = null;
      
      try {
         tx = session.beginTransaction();
         Query q = session.createNativeQuery("delete from propose where id in (select id from propose_dts where real_date_vol between '"+date1+"' and '"+date2+"')");
         q.executeUpdate();
         tx.commit();
      } catch (HibernateException e) {
         if (tx!=null) tx.rollback();
         throw e;
      } finally {
         session.close(); 
      }
   }
    public String save(Session session){//mila connexion ray
        Transaction tx = null;
        String ID = null;
        ID = (String) session.save(this);
        return ID;
    }
//     public List<Propose_dts> select(String date1, String date2) throws ParseException {
//        SessionFactory factory = HbUtil.getSessionFactory();
//        Session session = factory.openSession();
//        Transaction tx = null;
//        try {
//            tx = session.beginTransaction();
//            org.hibernate.query.Query query = session.createQuery("FROM Propose_dts p where p.real_date_vol between '" + date1 + "' and '" + date2 + "'");
//            List<Propose_dts> props = query.list();
//            tx.commit();
////            Propose[] array = props.toArray(new Propose[props.size()]);
//            return props;
//        } catch (HibernateException e) {
//            if (tx != null) {
//                tx.rollback();
//            }
//            throw e;
//        } finally {
//            session.close();
//        }
//    }
    
}

