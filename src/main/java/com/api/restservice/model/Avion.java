/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.restservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author mariano
 */
@Entity
public class Avion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) 
    private String id;
    @Column(name = "nom_avn")
    private String nomAvn;
    @Column(name = "id_mdl")
    private String idMdl;
    private int etat;
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNomAvn() {
        return nomAvn;
    }

    public void setNomAvn(String nomAvn) {
        this.nomAvn = nomAvn;
    }

    public String getIdMdl() {
        return idMdl;
    }

    public void setId_mdl(String id_mdl) {
        this.idMdl = id_mdl;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }
    
    
}
