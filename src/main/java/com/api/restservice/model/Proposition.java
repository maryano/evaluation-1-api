/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools ; 
 Templates
 * and open the template in the editor.
 */
package com.api.restservice.model;

import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import org.hibernate.Session;
import org.hibernate.annotations.Immutable;
import org.joda.time.Duration;
import org.joda.time.Instant;
import org.joda.time.Interval;
import util.*;

/**
 *
 * @author mariano
 */
@Entity
@Immutable
@IdClass(PropositionId.class)
@Table(name = "list_vol_piste_dts")
public class Proposition implements Serializable {

    @Id
    private String id;
    @Id
    @Column(name="idPst")
    private String idPst;
    @Column(name="date_vol")
    private Timestamp dateVol;
    @Column(name="decalage_vol")
    private Time decalageVol;
    @Column(name="etat_decalage_vol")
    private int etatDecalageVol;
    @Column(name="etat_vol")
    private int etatVol;
    @Column(name="type_vol")
    private int typeVol;
    @Column(name="id_tr")
    private String idTr;
    @Column(name="id_avn")
    private String idAvn;
    @Column(name="nom_avn")
    private String nomAvn;
    @Column(name="id_mdl")
    private String idMdl;
    @Column(name="nom_mdl")
    private String nomMdl;
    @Column(name="longueur_mdl")
    private double longueurMdl;
    @Column(name="longueur_pst")
    private double longueurPst;
    private String numero;
    @Column(name="temps_degagement_pst")
    private Time tempsDegagementPst;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdPst() {
        return idPst;
    }

    public void setIdPst(String idPst) {
        this.idPst = idPst;
    }

    public Timestamp getDateVol() {
        return dateVol;
    }

    public void setDateVol(Timestamp dateVol) {
        this.dateVol = dateVol;
    }

    public Time getDecalageVol() {
        return decalageVol;
    }

    public void setDecalageVol(Time decalageVol) {
        this.decalageVol = decalageVol;
    }

    public int getEtatDecalageVol() {
        return etatDecalageVol;
    }

    public void setEtatDecalageVol(int etatDecalageVol) {
        this.etatDecalageVol = etatDecalageVol;
    }

    public int getEtatVol() {
        return etatVol;
    }

    public void setEtatVol(int etatVol) {
        this.etatVol = etatVol;
    }

    public int getTypeVol() {
        return typeVol;
    }

    public void setTypeVol(int typeVol) {
        this.typeVol = typeVol;
    }

    public String getIdTr() {
        return idTr;
    }

    public void setIdTr(String idTr) {
        this.idTr = idTr;
    }

    public String getIdAvn() {
        return idAvn;
    }

    public void setIdAvn(String idAvn) {
        this.idAvn = idAvn;
    }

    public String getNomAvn() {
        return nomAvn;
    }

    public void setNomAvn(String nomAvn) {
        this.nomAvn = nomAvn;
    }

    public String getIdMdl() {
        return idMdl;
    }

    public void setIdMdl(String idMdl) {
        this.idMdl = idMdl;
    }

    public String getNomMdl() {
        return nomMdl;
    }

    public void setNomMdl(String nomMdl) {
        this.nomMdl = nomMdl;
    }

    public double getLongueurMdl() {
        return longueurMdl;
    }

    public void setLongueurMdl(double longueurMdl) {
        this.longueurMdl = longueurMdl;
    }

    public double getLongueurPst() {
        return longueurPst;
    }

    public void setLongueurPst(double longueurPst) {
        this.longueurPst = longueurPst;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Time getTempsDegagementPst() {
        return tempsDegagementPst;
    }

    public void setTempsDegagementPst(Time tempsDegagementPst) {
        this.tempsDegagementPst = tempsDegagementPst;
    }

    public Timestamp getRealDateVol() {
        return util.Util.addSectoTs(dateVol, decalageVol);
    }

    public String showDate_vol() {
        return util.Util.showDate(dateVol);
    }

    public String showRealDate_vol() {
        return util.Util.showDate(getRealDateVol());
    }

    public Interval getInterval() {
//        begin = date_vol + decalage_vol 
//        end = date_vol + decalage_vol + tempsDegagementPst
        Timestamp start_temp = Util.addSectoTs(this.getDateVol(), this.getDecalageVol());
        Timestamp end_temp = Util.addSectoTs(start_temp, this.getTempsDegagementPst());
        Instant start = new Instant(start_temp.getTime());
        Instant end = new Instant(end_temp.getTime());
        return new Interval(start, end);
    }

//    public List<Proposition> select(String date1, String date2)  {
//        SessionFactory factory = HbUtil.getSessionFactory();
//        Session session = factory.openSession();
////        Transaction tx = null;
////        try {
////            tx = session.beginTransaction();
//            Query query = session.createQuery("FROM Proposition p where p.date_vol between '" + date1 + "' and '" + date2 + "'");
//                List<Proposition> props = query.list();
////            tx.commit();
////            Proposition[] array = props.toArray(new Proposition[props.size()]);
//            return props;
////        } catch (HibernateException e) {
////            if (tx != null) {
////                tx.rollback();
////            }
////            throw e;
////        } finally {
////            session.close();
////        }
//    }
//    public select
//    if overlap : return overlap duration in s
//    if dont overlap : return null
    public Long overlaps(List<Proposition> props) throws Exception {
        // filtrer les proposidtion par piste
        //alaina ze mitovy piste amle vao ho inserena iverifiena anle date ho overlap ou pas 
        // indice[farany] no alaina satria efa trié par date io, ASC
        // date nle vol récent drindra fotsiny jerena hoe ovelap ou pas 
        List<Proposition> samePist = props.stream().filter(c -> c.getIdPst().equals(this.getIdPst())).collect(Collectors.toList());
        System.out.println("samePist for " + this.getId() + " : " + samePist.size() + " " + this.getIdPst());
        //ra tsy atao maj ze efa a de commentena ity
        //ignorena le decallage taloha ratsy zany tsy mandroso le vol décallé ra sendra niala le teo alohany nbloque azy
        this.setDecalageVol(new Time(0, 0, 0));
        if (samePist.size() == 0) {
            return null;
        }
        int lastOcc = samePist.size() - 1;
        Proposition last = samePist.get(lastOcc);
//      if current proposition`s intervall overlaps the most recent flight's intervall - > must decalle
//      nullena fotsiny le this.decallage avant test overlap de tsy pris en compte, ra anao maj
//        this.setDecalageVol(new Time(0, 0, 0));//ra tsy atao maj ze efa a de commentena ity
        Interval overlapDuration = last.getInterval().overlap(this.getInterval());
//         System.out.println("overlap intervall with last : "+overlapDuration);
//        condition if last.interval before this.intervall -> ok
//         if last.intervall after this;intervall -> duration = duration + this.get
        if (overlapDuration != null) {
            Duration duration = overlapDuration.toDuration();
            Long s = duration.getStandardSeconds();
            int res = 0;
//            IMPORTANT
            if (last.getRealDateVol().after(this.getDateVol())) {//efa nisy décalage ve i THIS?
                int degage = util.Util.timeTosec(this.getTempsDegagementPst());
                Long align = degage - s;
                s = degage + align;
            }
            return s;
        } else {

            if (last.getRealDateVol().after(this.getDateVol())) {
//                System.out.println("lasa any arina be an depart an " + last.getId()   );
//                System.out.println("lsst depart " + last.getRealDateVol());
//                System.out.println("my supposed depart " + this.getDateVol());
                Interval gap = last.getInterval().gap(this.getInterval());
//                ra tsisy espace entre le intreval roa nefa eo alohan'i last i this
                if (gap == null) {
                    return Long.valueOf("0");
                } else {
                    Duration duration = gap.toDuration();
                    Long gaps = duration.getStandardSeconds();
                    int degage = util.Util.timeTosec(this.getTempsDegagementPst());
                    Long align = degage + degage + gaps;
                    return align;
                }
            }
            return null;
        }
    }

    public void decaller(Long second) {
        int s = Integer.parseInt(second.toString());
        Time decal = new Time(0, 0, s);
        this.setDecalageVol(decal);
    }

    public void valider(Session session) throws Exception {
        Vol v = new Vol(this.getId(), this.getDateVol(), this.getDecalageVol(), this.getEtatDecalageVol(), this.getEtatVol(), this.getTypeVol(), this.getIdTr(), this.getIdAvn());
//set parameters aloha zay vo valider - ataovy zay aveo
        v.valider(this.getDecalageVol());
        Propose p = new Propose();
        p.setIdVol(this.getId());
        p.setIdPst(this.getIdPst());
        p.save(session);
        v.update(session);

    }
}
