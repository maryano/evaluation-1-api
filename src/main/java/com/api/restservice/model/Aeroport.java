/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.restservice.model;

/**
 *
 * @author mariano
 */
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author mariano
 */
public class Aeroport {

    public List<Proposition> proposer(List<Proposition> props) throws Exception{
        List<Proposition> result = new ArrayList();
        //implementer algo pour attribuer vol - piste 
        for (Proposition pr : props) {
            System.out.println("traitement de "+pr.getId()+" pour "+pr.getIdPst());
            // si le vol a déjà été programmé, on ne propose plus rien, il a déjà une piste
            if (result.stream().filter(c -> c.getId().equals(pr.getId())).collect(Collectors.toList()).size() == 0) {
                //verifie l'intersection avec la plus récente 
                Long second = pr.overlaps(result);
                if (second == null) {
                    result.add(pr);
//                    pr. tokony 00 zany le decal ao , settena mitsy
                    System.out.println(pr.getId()+" est programmé sans décallage à "+ pr.getRealDateVol() +" dans la piste "+pr.getIdPst());
                }else{//décalleo
                    pr.decaller(second);
                    result.add(pr);
                    System.out.println(pr.getId()+" requiert un décallage de "+pr.getDecalageVol()+" secondes pour piste "+pr.getIdPst()+ " " + pr.getRealDateVol());
                }

            } else {
//                Suis-je mieux?
                  //critère
//                    - ohatra hoe ra za no libre mialoha
              Proposition temp_propose = result.stream().filter(c -> c.getId().equals(pr.getId())).collect(Collectors.toList()).get(0);
              System.out.println(" is  "+ temp_propose.getIdPst() + " better than " + pr.getIdPst() + " for "+ pr.getId()+" ?");
              int second = util.Util.timeTosec(temp_propose.getDecalageVol());
              int n_second = 0;
              Long new_second = pr.overlaps(result);
              if(new_second != null){
                  n_second = Integer.parseInt(new_second.toString());
              }
              System.out.println(" programmé : "+second);
              System.out.println(" re-proposé : "+new_second);
              //ra null le second ; de tsy mieux zany pr ptdr
                  if( n_second < second ){
                      pr.decaller(new_second);
                      result.remove(temp_propose);
                      result.add(pr);
                      System.out.println(temp_propose.getId()+" remplacé par "+pr.getIdPst() );
                  }else{
                      System.out.println("not better");
                  }
              
              
            }
            System.out.println("---------------------------------------- \n");
        }
            return result;
    }
    public List<Proposition> proposer(String min, String max) throws Exception {
        min = util.Util.formatDateBegin(min); //ajoute 00:00:00 à la fin d'une date, utilise la date du jour si = ""
        max = util.Util.formatDateEnd(max);//ajoute 23:59:59 à la fin d'une date, utilise la date du jour si = ""
        Proposition p = new Proposition();
        List<Proposition> result = new ArrayList();
        //select * from list_vol_piste where date_vol between min and max
//        List<Proposition> props = p.select(min, max);
//        return proposer(props);
return null;
    }
}

