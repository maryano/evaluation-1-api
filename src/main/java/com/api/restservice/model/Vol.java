/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools ;
 Templates
 * and open the template in the editor.
 */
package com.api.restservice.model;


import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import util.HbUtil;

/**
 *
 * @author mariano
 */
@Entity
public class Vol implements Serializable {
//    private static final SimpleDateFormat dateFormat
//      = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    @NotNull
    @Column(name="date_vol")
    private Timestamp dateVol;
    @Column(name="decalage_vol")
    private Time decalageVol;
    @Column(name="etat_decalage_vol")
    private int etatDecalageVol;
    @Column(name="etat_vol")
    private int etatVol;
    @NotNull
    @Column(name="type_vol")
    private int typeVol;
    @NotNull
    @NotBlank
    @Column(name="id_tr")
    private String idTr;
    @NotNull
    @NotBlank   
    @Column(name="id_avn")
    private String idAvn;

    public Vol(String id, Timestamp dateVol, Time decalageVol, int etatDecalageVol, int etatVol, int typeVol, String idTr, String idAvn) {
        this.id = id;
        this.dateVol = dateVol;
        this.decalageVol = decalageVol;
        this.etatDecalageVol = etatDecalageVol;
        this.etatVol = etatVol;
        this.typeVol = typeVol;
        this.idTr = idTr;
        this.idAvn = idAvn;
    }
    public Vol(Timestamp dateVol, Time decalageVol, int etatDecalageVol, int etatVol, int typeVol, String idTr, String idAvn) {
        this.dateVol = dateVol;
        this.decalageVol = decalageVol;
        this.etatDecalageVol = etatDecalageVol;
        this.etatVol = etatVol;
        this.typeVol = typeVol;
        this.idTr = idTr;
        this.idAvn = idAvn;
    }

    public Vol() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Timestamp getDateVol() {
        return dateVol;
    }

    public void setDateVol(Timestamp dateVol) {
        this.dateVol = dateVol;
    }

    public Time getDecalageVol() {
        return decalageVol;
    }

    public void setDecalageVol(Time decalageVol) {
        this.decalageVol = decalageVol;
    }

    public int getEtatDecalageVol() {
        return etatDecalageVol;
    }

    public void setEtatDecalageVol(int etatDecalageVol) {
        this.etatDecalageVol = etatDecalageVol;
    }

    public int getEtatVol() {
        return etatVol;
    }

    public void setEtatVol(int etatVol) {
        this.etatVol = etatVol;
    }

    public int getTypeVol() {
        return typeVol;
    }

    public void setTypeVol(int typeVol) {
        this.typeVol = typeVol;
    }

    public String getIdTr() {
        return idTr;
    }

    public void setIdTr(String idTr) {
        this.idTr = idTr;
    }

    public String getIdAvn() {
        return idAvn;
    }

    public void setIdAvn(String idAvn) {
        this.idAvn = idAvn;
    }

    

    //fonctions
    public void Annuler() throws Exception {
//        EDIT
        this.setEtatVol(-1);
        try{

        this.update(); 
        }catch(Exception e){
            throw e;
        }
    }

//    à verifier
//    asina date
    public void Effectuer() throws Exception {
//        SESSION
        SessionFactory factory = HbUtil.getSessionFactory();
        Session session = factory.openSession();
        VolEffectif ve = new VolEffectif();
//        EDIT
        ve.setId_vol(this.id);
        this.setEtatVol(20);
        try{
//        insert in ve table;
        ve.save(null,session);
//        update state to 20;
        this.update(session); 
        }catch(Exception e){
            throw e;
        }finally{
            session.close();
        }
        
    }

    public void Decaler() {
        // mila temps de décalage fotsiny e
        // etat de décallage
        // save to vol, le temps de décallage
        
    }
    public void valider(Time decal){
        this.setDecalageVol(decal);
        this.setEtatVol(10);
    }
    public Vol[] select() throws Exception {
        SessionFactory factory = HbUtil.getSessionFactory();
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            List<Vol> vols = session.createQuery("FROM Vol").list();
            tx.commit();
            Vol[] array = vols.toArray(new Vol[vols.size()]);
            return array;
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            throw e;
        } finally {
            session.close();
        }
    }

    public void update()throws Exception {
        SessionFactory factory = HbUtil.getSessionFactory();
        Session session = factory.openSession();
        try {

            this.update(session);

        } catch (HibernateException e) {
            throw e;
        } finally {
            session.close();
        }
    }

    public void update(Session session) throws Exception {     
            session.update(this);  
    }
}
