/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.restservice.controller;

import com.api.restservice.model.Aeroport;
import com.api.restservice.model.Avion;
import com.api.restservice.model.Modele;
import com.api.restservice.model.Proposition;
import com.api.restservice.model.Trajet;
import com.api.restservice.model.Vol;
import com.api.restservice.model.VolInfo;
import com.api.restservice.playload.ApiResponse;
import com.api.restservice.playload.AvionRequest;
import com.api.restservice.playload.SearchAvion;
import com.api.restservice.playload.VolRequest;
import com.api.restservice.repository.AvionRepository;
import com.api.restservice.repository.ModeleAvnRepository;
import com.api.restservice.repository.PropositionRepository;
import com.api.restservice.repository.TrajetRepository;
import com.api.restservice.repository.VolInfoRepository;
import com.api.restservice.repository.VolRepository;
import java.net.URI;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author mariano
 */
@CrossOrigin
@RestController
@RequestMapping("api/backoffice")
public class BackOfficeController {

    @Autowired
    PropositionRepository propositionRepository;
    @Autowired
    AvionRepository avionRepository;
    @Autowired
    VolInfoRepository volInfoRepository;
    @Autowired
    VolRepository volRepository;
    @Autowired
    TrajetRepository trajetRepository;
    @Autowired
    ModeleAvnRepository modeleRepository;

//    VOL
    @Secured("ROLE_ADMIN")
    @PostMapping("/vols")
    public ResponseEntity<?> saveVol(@Valid @RequestBody VolRequest vol) throws Exception {
        String time = vol.getTime();
        String date = vol.getDateVol()  ;
        System.out.println("time : "+time);
        System.out.println("date bfr : "+date);
        if(time == null || time.equals("")){
            time = "00:00:00";
        }else{
            time = time+":00";
        }
        Timestamp onlyDate = Timestamp.valueOf(date+" "+time);
        
        Vol v = new Vol(onlyDate, new Time(0, 0, 0), 0, 0, vol.getTypeVol(), vol.getIdTr(), vol.getIdAvn());
        Vol result = volRepository.save(v);
        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/api/backoffice/vol/{id}}")
                .buildAndExpand(result.getId()).toUri();

        return ResponseEntity.created(location).body(new ApiResponse(200, true, "vol enregistré"));
    }

    @GetMapping(value = {"/vols"})
    Page<VolInfo> filterVol(@RequestParam(name = "d", required = false) String debut, @RequestParam(name = "f", required = false) String fin, @RequestParam(name = "tr", required = false) String trajet, @RequestParam(name = "avn", required = false) String avion, @RequestParam(name = "page", required = false) Integer page, @RequestParam(name = "sort", required = false) String sort) throws Exception {
        String min = util.Util.formatDateBeginExtrem(debut); //ajoute 00:00:00 à la fin d'une date, utilise la date du jour si = ""
        String max = util.Util.formatDateEndExtrem(fin);
        Timestamp d1 = Timestamp.valueOf(min);
        Timestamp d2 = Timestamp.valueOf(max);
        sort = sort == null ? "" : sort;
        avion = avion == null ? "" : avion;
        trajet = trajet == null ? "" : trajet;
        if (page == null) {
            if (sort.equals("desc")) {
                return volInfoRepository.findFilterwithoutType(d1, d2, trajet, avion, PageRequest.of(0, 10, Sort.by("date_vol").descending()));
            } else {
                return volInfoRepository.findFilterwithoutType(d1, d2, trajet, avion, PageRequest.of(0, 10, Sort.by("date_vol").ascending()));
            }
        } else {
            if (sort.equals("desc")) {
                return volInfoRepository.findFilterwithoutType(d1, d2, trajet, avion, PageRequest.of(page, 10, Sort.by("date_vol").descending()));
            } else {
                return volInfoRepository.findFilterwithoutType(d1, d2, trajet, avion, PageRequest.of(page, 10, Sort.by("date_vol").ascending()));
            }
        }

    }

    @PutMapping("/vols")
    public ResponseEntity<?> UpdateVol(@Valid @RequestBody Vol vol) throws Exception {
        Vol result = volRepository.save(vol);
        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/api/backoffice/vols/{id}}")
                .buildAndExpand(result.getId()).toUri();

        return ResponseEntity.created(location).body(new ApiResponse(200, true, "vol modifié"));
    }

    @PatchMapping("/vols/{id}")
    public ResponseEntity<?> DeleteVol( @PathVariable("id") String id) throws Exception {
        if(id != null){
         Vol dbOcc = volRepository.findById(id).get();
         dbOcc.setEtatVol(-1);
         Vol result = volRepository.save(dbOcc);
         return ResponseEntity.status(200).body(new ApiResponse(200, true, " vol supprimé"));
        }else{
            return ResponseEntity.badRequest().body(new ApiResponse(400, true, "badRequest"));
        }
    }

    //avion
    @Secured("ROLE_ADMIN")
    @PostMapping("/avions")
    public ResponseEntity<?> saveAvion(@Valid @RequestBody AvionRequest avion) throws Exception {
        Avion avn = new Avion();
        avn.setId_mdl(avion.getIdMdl());
        avn.setNomAvn(avion.getNomAvion());
        avn.setEtat(0);
        Avion result = avionRepository.save(avn);
        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/api/backoffice/avion/{id}}")
                .buildAndExpand(result.getId()).toUri();

        return ResponseEntity.created(location).body(new ApiResponse(200, true, "Avion enregistré"));
    }
    @GetMapping("/avions/filter")
    List<Avion> filterAvion(@RequestBody SearchAvion s) {
        return avionRepository.findFilter(s.getNomAvn(), s.getIdMdl());
    }
    
    public ResponseEntity<?> upload(){
        return null;
    }

    @GetMapping("/avions")
    List<Avion> getAvions() {
        return avionRepository.findAll();
    }
    
    //update avion (put)
    //delete avion (patch)

    //proposition
    @GetMapping("/propositions")
    List<Proposition> proposerPiste() throws Exception {
        List<Proposition> p = propositionRepository.findAll();
        Aeroport a = new Aeroport();
        return a.proposer(p);
    }  
    @GetMapping("/trajets")
    List<Trajet> getTrajet(){
        return trajetRepository.findAll();
    }
    @GetMapping("/modeles")
    List<Modele> getModele(){
        return modeleRepository.findAll();
    }
}
