/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.restservice.services;

import com.api.restservice.model.Vol;
import com.api.restservice.playload.SearchVol;
import com.api.restservice.repository.VolRepository;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

/**
 *
 * @author mariano
 */
public class VolService {
    @Autowired
    static VolRepository volRepository;
    public static List<Vol>searchVol(SearchVol s, Optional<Integer> page) throws Exception{
        String min = util.Util.formatDateBeginExtrem(s.getDebut()); //ajoute 00:00:00 à la fin d'une date, utilise la date du jour si = ""
        String max = util.Util.formatDateEndExtrem(s.getFin());
        Timestamp d1 = Timestamp.valueOf(min);
        Timestamp d2 = Timestamp.valueOf(max);
        
        if (!page.isPresent()) {
            if(s.getSort().equals("desc")){
                return volRepository.findFilterwithoutType(d1, d2, s.getIdTr(), s.getIdAvn(), PageRequest.of(0, 10, Sort.by("date_vol").descending())).getContent();
            }else{
                return volRepository.findFilterwithoutType(d1, d2, s.getIdTr(), s.getIdAvn(), PageRequest.of(0, 10, Sort.by("date_vol").ascending())).getContent();
            }
        } else {
            if(s.getSort().equals("desc")){
                return volRepository.findFilterwithoutType(d1, d2, s.getIdTr(), s.getIdAvn(), PageRequest.of(page.get(), 10,Sort.by("date_vol").descending())).getContent();
            }else{
                return volRepository.findFilterwithoutType(d1, d2, s.getIdTr(), s.getIdAvn(), PageRequest.of(page.get(), 10,Sort.by("date_vol").ascending())).getContent(); 
            }
        }
    }
}
