-- databASe aeroport
CREATE databASe tour_controle

DROP TABLE proposition;
DROP TABLE vol_historique;
DROP TABLE vol_effectif;
DROP TABLE vol;
DROP TABLE avion;
DROP TABLE modele;
DROP TABLE trajet;
DROP TABLE piste;

DROP SEQUENCE pr_seq;
DROP SEQUENCE tr_seq;
DROP SEQUENCE pst_seq;
DROP SEQUENCE vh_seq;
DROP SEQUENCE ve_seq;
DROP SEQUENCE vol_seq;
DROP SEQUENCE avn_seq;
DROP SEQUENCE mdl_seq;

CREATE SEQUENCE pr_seq;
CREATE SEQUENCE tr_seq;
CREATE SEQUENCE pst_seq;
CREATE SEQUENCE vh_seq;
CREATE SEQUENCE ve_seq;
CREATE SEQUENCE vol_seq;
CREATE SEQUENCE avn_seq;
CREATE SEQUENCE mdl_seq;
create sequence users_seq;

create table users(
    id serial PRIMARY KEY,
    name VARCHAR(30),
    phone VARCHAR(10),
    password VARCHAR(100),
    email varchar(20)
);

CREATE TABLE trajet(
	id VARCHAR(10) PRIMARY KEY DEFAULT to_char(nextval('tr_seq'::regclASs), '"TR"fm000000'::text) NOT NULL,
	ville_dep VARCHAR(30)
);

insert into trajet (ville_dep) values ('Paris');
insert into trajet (ville_dep) values ('Dubaï');
insert into trajet (ville_dep) values ('Californie');
insert into trajet (ville_dep) values ('Suisse');
insert into trajet (ville_dep) values ('Allemagne');

CREATE TABLE piste(
	id VARCHAR(10) PRIMARY KEY DEFAULT to_char(nextval('pst_seq'::regclASs), '"PST"fm000000'::text) NOT NULL,
    numero VARCHAR(30),
	longueur_pst float,
	temps_degagement_pst TIME
);
 
insert into piste (numero,longueur_pst,temps_degagement_pst) values ('1',2000,'00:20:00') ;    
insert into piste (numero,longueur_pst,temps_degagement_pst) values ('2',1000,'00:20:00') ;    
insert into piste (numero,longueur_pst,temps_degagement_pst) values ('3',500,'00:20:00') ;    
-- insert into piste (numero,longueur_pst,temps_degagement_pst) values ('2',2000,'00:12:00') ;    

CREATE TABLE modele(
	id VARCHAR(10) PRIMARY KEY DEFAULT to_char(nextval('mdl_seq'::regclASs), '"MDL"fm000000'::text) NOT NULL,
	nom_mdl VARCHAR(40),
	longueur_mdl float
);

insert into modele (nom_mdl,longueur_mdl) values('A150',1500);
insert into modele (nom_mdl,longueur_mdl) values('A150B',1500);
insert into modele (nom_mdl,longueur_mdl) values('A080',800);
insert into modele (nom_mdl,longueur_mdl) values('MIG21',1500);

CREATE TABLE avion(
	id VARCHAR(10) PRIMARY KEY DEFAULT to_char(nextval('avn_seq'::regclASs), '"AVN"fm000000'::text) NOT NULL,
	nom_avn VARCHAR(40),
	id_mdl VARCHAR(10) REFERENCES modele (id)
);
alter table avion add column etat int;
alter table avion add column img varchar(150);
update avion set etat = 0;

insert into avion (nom_avn, id_mdl) values ('ra15001', 'MDL000007');
insert into avion (nom_avn, id_mdl) values ('ra15002', 'MDL000008');
insert into avion (nom_avn, id_mdl) values ('ra800', 'MDL000009');
insert into avion (nom_avn, id_mdl) values ('Mig 21', 'MDL000010');

CREATE TABLE vol(
	id VARCHAR(10) PRIMARY KEY DEFAULT to_char(nextval('vol_seq'::regclASs), '"VOL"fm000000'::text) NOT NULL,
    date_vol TIMESTAMP,
	decalage_vol TIME,
	etat_decalage_vol INT, -- en avance ou en retard -1 ou 1 / 0 par defaut;
	etat_vol INT,-- annuler ou pAS -1 annuler / 0 par defaut / validé : 10 / effectif : 20
	type_vol INT, -- sort ou entre dans l'aeroport -1 ou 1 (-1 si sort)
	id_tr VARCHAR(10) REFERENCES trajet (id),
	id_avn VARCHAR(10) REFERENCES avion (id)
);

-- insert into vol (date_vol, decalage_vol, etat_decalage_vol, etat_vol, type_vol, id_tr, id_avn) values ('2020-09-04 00:10:00','00:00:00', 0, 0, 1, 'TR000001','AVN000001') ;
-- insert into vol (date_vol, decalage_vol, etat_decalage_vol, etat_vol, type_vol, id_tr, id_avn) values ('2020-09-04 16:30:00','00:00:00', 0, 0, 1, 'TR000002','AVN000002') ;
-- insert into vol (date_vol, decalage_vol, etat_decalage_vol, etat_vol, type_vol, id_tr, id_avn) values ('2020-09-04 16:30:00','00:00:00', 0, 0, 1, 'TR000003','AVN000003') ;
-- insert into vol (date_vol, decalage_vol, etat_decalage_vol, etat_vol, type_vol, id_tr, id_avn) values ('2020-09-05 16:30:00','00:00:00', 0, 0, -1, 'TR000004','AVN000003') ;
-- insert into vol (date_vol, decalage_vol, etat_decalage_vol, etat_vol, type_vol, id_tr, id_avn) values ('2020-09-05 16:30:00','00:00:00', 0, 0, -1, 'TR000004','AVN000001') ;
insert into vol (date_vol, decalage_vol, etat_decalage_vol, etat_vol, type_vol, id_tr, id_avn) values ('2020-09-09 12:00:00','00:00:00', 0, 0, -1, 'TR000004','AVN000004') ;

insert into vol (date_vol, decalage_vol, etat_decalage_vol, etat_vol, type_vol, id_tr, id_avn) values ('2020-09-09 12:05:00','00:00:00', 0, 0, -1, 'TR000004','AVN000004') ;
insert into vol (date_vol, decalage_vol, etat_decalage_vol, etat_vol, type_vol, id_tr, id_avn) values ('2020-09-09 12:10:00','00:00:00', 0, 0, -1, 'TR000004','AVN000006') ;
insert into vol (date_vol, decalage_vol, etat_decalage_vol, etat_vol, type_vol, id_tr, id_avn) values ('2020-09-09 12:15:00','00:00:00', 0, 0, -1, 'TR000004','AVN000005') ;

insert into vol (date_vol, decalage_vol, etat_decalage_vol, etat_vol, type_vol, id_tr, id_avn) values ('2020-09-09 12:20:00','00:00:00', 0, 0, -1, 'TR000004','AVN000006') ;
insert into vol (date_vol, decalage_vol, etat_decalage_vol, etat_vol, type_vol, id_tr, id_avn) values ('2020-09-09 12:25:00','00:00:00', 0, 0, -1, 'TR000004','AVN000006') ;
insert into vol (date_vol, decalage_vol, etat_decalage_vol, etat_vol, type_vol, id_tr, id_avn) values ('2020-09-09 12:30:00','00:00:00', 0, 0, -1, 'TR000004','AVN000004') ;
insert into vol (date_vol, decalage_vol, etat_decalage_vol, etat_vol, type_vol, id_tr, id_avn) values ('2020-09-09 12:35:00','00:00:00', 0, 0, -1, 'TR000004','AVN000006') ;

insert into vol (date_vol, decalage_vol, etat_decalage_vol, etat_vol, type_vol, id_tr, id_avn) values ('2020-09-09 12:40:00','00:00:00', 0, 0, -1, 'TR000004','AVN000005') ;
insert into vol (date_vol, decalage_vol, etat_decalage_vol, etat_vol, type_vol, id_tr, id_avn) values ('2020-09-09 12:45:00','00:00:00', 0, 0, -1, 'TR000004','AVN000005') ;
insert into vol (date_vol, decalage_vol, etat_decalage_vol, etat_vol, type_vol, id_tr, id_avn) values ('2020-09-30 08:00:00','00:00:00', 0, 0, -1, 'TR000003','AVN000004') ;

CREATE TABLE vol_historique(
	id VARCHAR(10) PRIMARY KEY DEFAULT to_char(nextval('vh_seq'::regclASs), '"VH"fm000000'::text) NOT NULL,
	date_vh TIMESTAMP,
	id_vol VARCHAR(10) REFERENCES vol (id)
);

CREATE TABLE vol_effectif(
	id VARCHAR(10) PRIMARY KEY DEFAULT to_char(nextval('ve_seq'::regclASs), '"VE"fm000000'::text) NOT NULL,
	date_effectif TIMESTAMP,
	id_vol VARCHAR(10) REFERENCES vol (id)
);

CREATE TABLE propose(
	id VARCHAR(10) PRIMARY KEY DEFAULT to_char(nextval('pr_seq'::regclASs), '"PR"fm000000'::text) NOT NULL,
	id_vol VARCHAR(10) REFERENCES vol (id),
	id_pst VARCHAR(10) REFERENCES piste (id)
);

-- vols valides (non annulés et non Effectifs)
-- get >= 0 to skip cancelled flight 
-- get < 10 to skip effectives 
CREATE OR REPLACE view vol_on AS 
    SELECT * FROM vol 
    WHERE etat_vol >= 0 AND etat_vol < 20;

-- AVION COMPLET
CREATE view avion_info AS 
    SELECT avion.*,nom_mdl,longueur_mdl FROM avion 
    JOIN modele 
    ON avion.id_mdl = modele.id;

-- vol + avion complet
CREATE OR REPLACE view vol_info AS 
    SELECT vol_on.*,nom_avn, id_mdl, nom_mdl ,longueur_mdl FROM vol_on 
    JOIN avion_info 
    ON vol_on.id_avn = avion_info.id;

-- liste vol piste
-- lie les vols dispos avec toutes les piste adaptés à l'avion 
create or replace view list_vol_piste as SELECT vol_info.*, piste.id as id_pst,numero,longueur_pst,temps_degagement_pst,pstx,psty,pstx2,psty2 FROM vol_info JOIN piste ON  piste.longueur_pst >= vol_info.longueur_mdl ;
-- UPDATE 


-- OCCURENCE VOL PISTE
create view vol_occ as select id,count(id) from list_vol_piste group by id;

-- VOL_PISTE WITH NUMBER OF OCCURENCE, this HELP WITH PRIORITY;
-- first date, first selected
-- give priority to count min
-- create or replace view list_vol_piste_dts as select list_vol_piste.*,count as occ from list_vol_piste join vol_occ on vol_occ.id = list_vol_piste.id order by date_vol,count;
create or replace view list_vol_piste_dts as select list_vol_piste.*,count,x as trX,y as trY ,ville_dep as occ from list_vol_piste join vol_occ on vol_occ.id = list_vol_piste.id join trajet on trajet.id = list_vol_piste.id_tr order by count,date_vol;

-- //get proposistion by date
-- create view

create view propose_dts as select propose.*, (date_vol+decalage_vol) as real_date_vol from propose join vol on propose.id_vol = vol.id ;


-- vol + avion + trajet
create view vol_avn_tr as select vol_info.*,ville_dep,
	CASE WHEN type_vol = 0 THEN 'décollage'
		ELSE 'atterissage' END
         AS typeVolLabel
 from vol_info join trajet on vol_info.id_tr = trajet.id;


alter table trajet add column x float;
alter table trajet add column y float;

alter table piste add column pstX float;
alter table piste add column pstX2 float;
alter table piste add column pstY2 float;
alter table piste add column pstY float;